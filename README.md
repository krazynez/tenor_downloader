## Setup

1.) Install the python3 requirements with pip(pip3)

`pip3 -r requirements.txt`

2.) Get the [GeckoDriver](https://github.com/mozilla/geckodriver/releases) add it to your PATH

3.) Change permissions on run.py

`chmod +x run.py`


## Running

1.) No options: Get the top results

`./run.py`

2.) Search for GIF single word

`./run.py -s|--search <gif_name>`

3.) Search for GIF multi word

`./run.py -s|--search "<gif_name_with_spaces>"`


### Future plans

- **Windows** support

- Make faster search results

