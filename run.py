#!/usr/bin/python3
import requests
import time
from sys import platform
import argparse
from selenium import webdriver
from selenium.common.exceptions import StaleElementReferenceException

def viewer(image=None):
    #TODO: fix formatting of images
    img = ''
    for i in image:
        img += f'<img src="{i}"/>'
    images = f"""<!DOCTYPE html>
       <head><title>Tenor Gif Downloader</title></head>
       <body>{img}</body>
       </html>"""
       
    images = bytes(images, encoding='UTF-8')

    #TODO: Add Windows support
    if platform == "linux" or platform == "linux32" or platform == "darwin":
        f = open('/tmp/index.html', 'wb')
    else:
        print('Sorry right now Windows is not supported.')
        exit()
    

    f.write(images)
    f.close()

    filename = f'file:///tmp/index.html'

    return filename

def get_gif(search=None):
    options = webdriver.FirefoxOptions()
    options.add_argument('--headless')
    driver = webdriver.Firefox(options=options)
    
    if search is False:
        driver.get('https://tenor.com/')
    elif ' ' in search:
        search = search.replace(' ', '-')
        driver.get(f'https://tenor.com/search/{search}-gifs')
    else:
        driver.get(f'https://tenor.com/search/{search}-gifs')

    time.sleep(2)
    image_container = []
    driver.execute_script("window.scrollTo(0,document.body.scrollHeight)")
    time.sleep(2)
    # Needs to be below the scrolling or DOM Error will happen
    imgs = driver.find_elements_by_tag_name('img')
    try:
        print('Getting images.')
        count = 0
        for img in imgs:
            if img.get_attribute('src').endswith('.gif'):
                image_container.append(img.get_attribute('src'))
                count += 1
        print(f"Found {count} GIF's")
        mini_driver = webdriver.Firefox()
        mini_driver.get(viewer(image_container))
    except StaleElementReferenceException:
        print('Not in DOM')
    driver.quit()

def main():
    try:
        parser = argparse.ArgumentParser(description='Search for specific gifs')
        parser.add_argument('-s', '--search', nargs='?', action='store', default=False)
        args = parser.parse_args()
        get_gif(args.search)

    except KeyboardInterrupt:
        print('\nQuitting...')

if __name__ == '__main__':
    main()
